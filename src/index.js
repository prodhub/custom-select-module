class CustomSelect {
	constructor(options) {

		this._polyfill();
		Object.assign(this._options = {}, this._default(), options);

	}

	_default() {
		return {

			selector: 'select',
			showInputLength: 10,
			hideSelected: false,
			hideDisabled: false,

			onInit: function (select) {
			},
			onSelect: function (select, val) {
			},
			onKeyUp: function (select, val) {
			},

			templateInner: `<div class="{select}"></div>
							<div class="{value}"></div>
							<div class="{dropDown}">
								<div class="{input}"></div>
								<div class="{list}"></div>
							</div>`,

			className: {
				closed: 'select-closed',
				wrap: 'select-shell',
				select: 'select-shell__item',
				value: 'select-shell__value',
				input: 'select-shell__input',
				dropDown: 'select-shell__dropdown',
				list: 'select-shell__list',
				listItem: 'select-shell__list-item'
			}
		}
	}


	_templateToHTML(template, data) {
		return template ? template.replace(/\{([^\}]+)\}/g, function (value, key) {
			return key in data ? data[key] : value;
		}).replace(/&([^\=]+)\=\{([^\}]+)\}/g, '') : '';
	}


	_build() {
		let sel = document.querySelectorAll(this._options.selector);

		for (let i = 0; i < sel.length; i++) {
			let wrapper = document.createElement('div'),
				select = sel[i],
				templateInner = this._templateToHTML(this._options.templateInner, this._options.className);

			wrapper.dataset.hideSelected = !!this._options.hideSelected;
			wrapper.dataset.hideDisabled = !!this._options.hideDisabled;

			if (select.tagName !== 'SELECT') {
				console.error('Element must be select');
				return false;
			}
			if (select.closest('.' + this._options.className.wrap)) {
				console.error('Element is exist already');
				return false;
			}

			wrapper.className = this._options.className.wrap;
			select.parentNode.insertBefore(wrapper, select);
			wrapper.innerHTML = templateInner;
			select.remove();

			wrapper.getElementsByClassName(this._options.className.select)[0].appendChild(select);
			wrapper.getElementsByClassName(this._options.className.input)[0].innerHTML = '<input type="text">';

			this.update(select);
			this._eventListener(wrapper);

			if (this._options.onInit && typeof this._options.onInit === 'function') this._options.onInit(select);
		}
	}

	update(sel) {

		let select = typeof sel == 'string' ? document.querySelector(sel)[0] : sel,
			wrapper = select.closest('.' + this._options.className.wrap),
			list = wrapper.getElementsByClassName(this._options.className.list)[0],
			dropDown = wrapper.getElementsByClassName(this._options.className.dropDown)[0],
			input = wrapper.getElementsByTagName('input')[0];

		if (this._options.showInputLength && select.options.length < this._options.showInputLength) {
			wrapper.getElementsByClassName(this._options.className.input)[0].style.display = 'none';
		} else {
			wrapper.getElementsByClassName(this._options.className.input)[0].style.display = '';
		}

		wrapper.classList.add(this._options.className.closed);
		list.innerHTML = '';

		for (let i = 0; i < select.options.length; i++) {
			let option = select.options[i],
				cls = option.selected ? 'selected' : ( option.disabled ? 'disabled' : '' ),
				item = document.createElement('div');

			item.className = this._options.className.listItem + ' ' + cls;
			item.innerHTML = option.innerHTML;

			if (!(option.selected && wrapper.dataset.hideSelected == 'true' || option.disabled && wrapper.dataset.hideDisabled == 'true')) {
				list.appendChild(item);
			}
			if (!option.disabled) {
				item.addEventListener('click', () => {
					select.value = option.value;
					this.update(select);
					if (this._options.onSelect && typeof this._options.onSelect === 'function') this._options.onSelect(select, option.value, option.innerText);
				}, false);
			}
			if (option.selected) wrapper.getElementsByClassName(this._options.className.value)[0].innerHTML = option.innerHTML;
		}

		input.value = '';
	}

	detach(sel) {
		let select = typeof sel == 'string' ? document.querySelector(sel) : sel,
			wrapper = select.closest('.' + this._options.className.wrap);

		wrapper.innerHTML = '';
		wrapper.appendChild(select);

		var parent = wrapper.parentNode;
		while (wrapper.firstChild) parent.insertBefore(wrapper.firstChild, wrapper);
		parent.removeChild(wrapper);

		select.removeEventListener('change', ()=> {
		}, false);

	}

	_eventListener(wrapper) {
		let select = wrapper.querySelector(this._options.selector),
			input = wrapper.getElementsByTagName('input')[0],
			value = wrapper.getElementsByClassName(this._options.className.value)[0],
			dropDown = wrapper.getElementsByClassName(this._options.className.dropDown)[0],
			item = wrapper.getElementsByClassName(this._options.className.listItem);

		select.addEventListener('change', this.update(select), false);

		input.addEventListener('keyup', () => {
			for (let i = 0; i < item.length; i++) {
				let option = item[i];
				if (option.innerText.toLowerCase().indexOf(input.value.trim()) < 0) {
					option.style.display = 'none';
				} else {
					option.style.display = '';
				}
			}
			if (this._options.onKeyUp && typeof this._options.onKeyUp === 'function') this._options.onKeyUp(select, input.value);
		}, false);

		value.addEventListener('click', () => {

			if (wrapper.classList.contains(this._options.className.closed)) {
				wrapper.classList.remove(this._options.className.closed);
				if (select.options.length >= this._options.showInputLength) {
					input.focus();
				}
			} else {
				wrapper.classList.add(this._options.className.closed);
			}

		}, false);

		document.addEventListener('mouseup', (e)=> {
			if (e.target.closest('.' + this._options.className.wrap) !== wrapper) wrapper.classList.add(this._options.className.closed);
		}, false);

	}

	init(options) {
		Object.assign(this._options, this._default(), options);
		this._build();
	}

	_polyfill() {
		//matches
		if (!Element.prototype.matches) {
			Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector;
		}

		//closest
		if (!Element.prototype.closest) {
			Element.prototype.closest = function (css) {
				let node = this;
				while (node) {
					if (node.matches(css)) return node;
					else node = node.parentElement;
				}
				return null;
			};
		}

		//assign
		if (!Object.assign) {
			Object.defineProperty(Object, 'assign', {
				enumerable: false,
				configurable: true,
				writable: true,
				value: function (target, firstSource) {

					if (target === undefined || target === null) {
						throw new TypeError('Cannot convert first argument to object');
					}

					let to = Object(target);
					for (let i = 1; i < arguments.length; i++) {
						let nextSource = arguments[i];
						if (nextSource === undefined || nextSource === null) {
							continue;
						}

						let keysArray = Object.keys(Object(nextSource));
						for (let nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
							let nextKey = keysArray[nextIndex],
								desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
							if (desc !== undefined && desc.enumerable) {
								to[nextKey] = nextSource[nextKey];
							}
						}
					}
					return to;
				}
			});
		}


		// classList


		// IE8/9, Safari
		if (!('classList' in Element.prototype)) {

			let regExp = function (name) {
				return new RegExp('(^| )' + name + '( |$)');
			};
			let forEach = function (list, fn, scope) {
				for (let i = 0; i < list.length; i++) {
					fn.call(scope, list[i]);
				}
			};

			// class list object with basic methods
			function ClassList(element) {
				this.element = element;
			}

			ClassList.prototype = {
				add: function () {
					forEach(arguments, function (name) {
						if (!this.contains(name)) {
							this.element.className += ' ' + name;
						}
					}, this);
				},
				remove: function () {
					forEach(arguments, function (name) {
						this.element.className =
							this.element.className.replace(regExp(name), '');
					}, this);
				},
				toggle: function (name) {
					return this.contains(name)
						? (this.remove(name), false) : (this.add(name), true);
				},
				contains: function (name) {
					return regExp(name).test(this.element.className);
				},
				// bonus..
				replace: function (oldName, newName) {
					this.remove(oldName), this.add(newName);
				}
			};


			Object.defineProperty(Element.prototype, 'classList', {
				get: function () {
					return new ClassList(this);
				}
			});

			// replace() support for others
			if (window.DOMTokenList && DOMTokenList.prototype.replace == null) {
				DOMTokenList.prototype.replace = ClassList.prototype.replace;
			}
		}
	}

}

module.exports = CustomSelect;