# Custom select module  

Custom select module  
Example: http://custom-select.module.frontend.production.adwatch.ru

## Install  


```sh
$ npm install --save @adwatch/custom-select
```

## Usage  

```js
import CustomSelect from '@adwatch/custom-select';		// for es6

var CustomSelect = require('@adwatch/custom-select/build');	// for es5


let customSelect = new CustomSelect(options);

```

#### Include styles 

```scss
//_customSelect.scss

@import '../../node_modules/@adwatch/custom-select/src/customSelect';
```

## Get started

```js
let customSelect = new CustomSelect(options);
```



## API

#### Options	


| Name                       |   Type     |  Description |
| :------------------------- | :--------- | :------ |
| `selector      `           | `string `          | Select element selector, id of class. Default - `select` |
| `showInputLength   `       | `number / false `  | Length for autocomplete input show. Default - `10` |
| `hideSelected   `          | `boolean `         | Hide selected option item in dropdown list. Default - `false` |
| `hideDisabled   `          | `boolean `         | Hide disabled option item in dropdown list. Default - `false` |
| `template                ` | `string`   | Wrapper layout template*. |
| `className               ` | `object`   | Wrapper class names** |
| `onInit                  ` | `function` | Inited callback. |
| `onSelect                ` | `function` | Item on select callback. |
| `onKeyUp         `         | `function` | Autocomplete input key up callback.  |


\* Default template:
```html
<div class="{select}"></div>
<div class="{value}"></div>
<div class="{dropDown}">
	<div class="{input}"></div>
	<div class="{list}"></div>
</div>
```

\** Default className:

```js
{
	closed: 'select-closed',			// class added on closed dropdown  
	wrap: 'select-shell',				// all wrapper class
	select: 'select-shell__item',		// select wrapper class
	value: 'select-shell__value',		// value wrapper class
	input: 'select-shell__input',		// input wrapper class
	dropDown: 'select-shell__dropdown',	// dropdown wrapper class
	list: 'select-shell__list',			// option list wrapper class
	listItem: 'select-shell__list-item'	// option item wrapper class
}
```


### Callbacks option arguments

onInit arguments: `(current_select)`  
onSelect arguments: `(current_select, select_value)`  
onKeyUp arguments: `(current_select, select_value)`




## Public methods	

#### customSelect.init(options)
Inited plugin.

#### customSelect.update(select)
Update plugin.

#### customSelect.detach(select)
Detach plugin.




## License

MIT © 
